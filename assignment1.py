import re
import sys

out_file = open(sys.argv[2],"w")
with open(sys.argv[1]) as input_file:
 for line in input_file:
  line = line.strip()
  tokens = re.split("\\s", line)
  out_file.write(str(len(tokens)) + "\n")
out_file.close()
